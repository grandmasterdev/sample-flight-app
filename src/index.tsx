import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloProvider } from 'react-apollo';
import EnvHelper from './helpers/EnvHelper';

const env: string = EnvHelper.getEnvironment();
console.log('env',env);
const rapidFlightApiEndpoint: string = require('./configs/rapid-flight-api.json').environment[env].endpoint;

const httpLink = createHttpLink({
    uri: rapidFlightApiEndpoint,
  });
  
  const client = new ApolloClient({
    cache: new InMemoryCache(),
    link: httpLink,
  });

ReactDOM.render(<ApolloProvider client={client}><App /></ApolloProvider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
