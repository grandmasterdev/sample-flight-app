export default class EnvHelper {
    public static getEnvironment() {
        if (process && process.env && process.env.REACT_APP_ENV) {
            switch(process.env.REACT_APP_ENV){
                case 'development':
                return 'dev';
            }
        }

        return 'local';
    }
}