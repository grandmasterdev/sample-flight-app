import React, { Component, Fragment } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Chip from '@material-ui/core/Chip';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import moment from 'moment';

const CustomTableCell = withStyles(theme => ({
    head: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);

export default class FlightsCheapestCalendar extends Component<any, any, any> {
    constructor(props: any) {
        super(props);

        this.state = {
            originPlace: '',
            destinationPlace: ''
        }
    }

    componentWillReceiveProps(newProps: any) {
        console.log('oldProps', this.props);
        if (this.props !== newProps) {
            // Set travel routes
            const travelRoutes: string[] = newProps.travelRoute ? newProps.travelRoute.split('-') : [];
            console.log(travelRoutes);
            if(travelRoutes) {
                this.setState({
                    originPlace: travelRoutes[0] + '-sky',
                    destinationPlace: travelRoutes[1] + '-sky'
                })
            }
        }
    }

    renderEmptyCells(count: number, cellsOutput: any[]) {
        for (let i: number = 0; i < count; i++) {
            cellsOutput.push(
                <CustomTableCell key={'empty-' + i} className="App-content-date-empty">&nbsp;</CustomTableCell>
            )
        }
    }

    renderCell(data: any, index: number, cellsOutput: any[]) {
        cellsOutput.push(
            <CustomTableCell key={index} className="App-content-cell">
                <h2>{'$ ' + data.CheapestOfTheDay.CheapestPrice}</h2>
                <Chip label={data.CheapestOfTheDay.Agents.length + ' agents'} />
                <h4 className="App-content-date">{moment(data.OutboundDate).format('DD MMMM')}</h4>
            </CustomTableCell>
        )
    }

    renderRow() {
        const originPlace: string = this.state.originPlace;
        const destinationPlace: string = this.state.destinationPlace;
        const GET_FLIGHTS: any = gql`
        query Flights($originPlace: String!, $destinationPlace: String!) {
            search(originPlace:$originPlace,destinationPlace:$destinationPlace) {
            SessionKey,
            Flights {
              OutboundDate,
              CheapestOfTheDay {
                CheapestPrice,
                Agents {
                  Id,
                  Name,
                  ImageUrl
                }
              }
            }
          }
        }`;

        return (<Query query={GET_FLIGHTS} variables={{ originPlace, destinationPlace }}>
            {({ loading, error, data }) => {
                if (loading) return <p>Loading ...</p>
                if (error) return <p>Error :(</p>

                let initial: boolean = true;
                let rowCounter: number = 0;
                let rowOutput: any[] = [];
                let cellsOutput: any[] = [];

                const flights: any[] = data.search.Flights;
                console.log(flights);

                flights.map((data: any, index: number) => {
                    if ((flights.length - index - 1 < 7) && rowOutput.length >= 5) {
                        this.renderCell(data, index, cellsOutput);
                    } else if (flights.length - index - 1 <= 0) {
                        const balanceCells: number = 7 - rowCounter;
                        console.log(balanceCells);

                        // Push empty cells
                        this.renderEmptyCells(balanceCells, cellsOutput);

                        rowCounter = 0;

                        rowOutput.push(
                            <TableRow key={index}>
                                {cellsOutput}
                            </TableRow>
                        )

                        cellsOutput = [];
                    } else if (rowCounter === 0 && initial === true) {
                        switch (moment(data.OutboundDate).format('dddd')) {
                            case 'Sunday':
                                this.renderCell(data, index, cellsOutput);
                                break;
                            case 'Monday':
                                this.renderEmptyCells(1, cellsOutput);
                                this.renderCell(data, index, cellsOutput);

                                rowCounter = rowCounter + 1;
                                break;
                            case 'Tuesday':
                                this.renderEmptyCells(2, cellsOutput);
                                this.renderCell(data, index, cellsOutput);

                                rowCounter = rowCounter + 2;
                                break;
                            case 'Wednesday':
                                this.renderEmptyCells(3, cellsOutput);
                                this.renderCell(data, index, cellsOutput);

                                rowCounter = rowCounter + 3;
                                break;
                            case 'Thursday':
                                this.renderEmptyCells(4, cellsOutput);
                                this.renderCell(data, index, cellsOutput);

                                rowCounter = rowCounter + 4;
                                break;
                            case 'Friday':
                                this.renderEmptyCells(5, cellsOutput);
                                this.renderCell(data, index, cellsOutput);

                                rowCounter = rowCounter + 5;
                                break;
                            case 'Saturday':
                                this.renderEmptyCells(6, cellsOutput);
                                this.renderCell(data, index, cellsOutput);

                                rowCounter = rowCounter + 6;
                                break;
                        }

                        initial = false;
                    } else if ((rowCounter < 7 && initial === false)) {
                        this.renderCell(data, index, cellsOutput);
                    } else {
                        rowCounter = 0;

                        rowOutput.push(
                            <TableRow key={index}>
                                {cellsOutput}
                            </TableRow>
                        )

                        cellsOutput = [];

                        this.renderCell(data, index, cellsOutput);
                    }

                    rowCounter++;
                });

                console.log('row counter', rowCounter);

                return rowOutput;
            }}
        </Query>)
    }

    render() {
        const tableBody: any = this.renderRow() || '';
        console.log(tableBody);

        return (
            <Fragment>
                {tableBody}
            </Fragment>
        )
    }
}