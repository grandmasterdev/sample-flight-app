import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Chip from '@material-ui/core/Chip';
import './App.css';
import moment from 'moment';
import { FormControl, InputLabel, Select, OutlinedInput, MenuItem } from '@material-ui/core';
import FlightsCheapestCalendar from './components/FlightsCheapestCalendar';

const mockData: any = require('./configs/mock-data.json');

const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

class App extends Component<any, any, any> {

  constructor(props: any) {
    super(props);

    this.state = {
      route: '',
      labelWidth: 0
    }
  }

  componentDidMount() {
  }

  handleRouteChange = (event: any) => {
    this.setState({
      route: event.target.value
    })
  }

  renderRouteSelection() {
    return (
      <FormControl variant="outlined" >
        <InputLabel
          ref={(ref: any) => {
            const thisRef: any = this;

            thisRef.InputLabelRef = ref;
          }}
          htmlFor="outlined-travel-route-simple"
          className="App-content-select"
        >
          Select a travel route
          </InputLabel>
        <Select
          value={this.state.route}
          onChange={this.handleRouteChange}
          input={
            <OutlinedInput
              labelWidth={this.state.labelWidth}
              name="travel-route"
              id="outlined-travel-route-simple"
            />
          }
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          <MenuItem value='SIN-KUL'>SIN - KUL</MenuItem>
          <MenuItem value='KUL-SIN'>KUL - SIN</MenuItem>
          <MenuItem value='KUL-SFO'>KUL - SFO</MenuItem>
        </Select>
      </FormControl>
    )
  }

  render() {
    // const tableBody: any = this.renderRow();
    const routeSelection: any = this.renderRouteSelection();

    return (
      <div className="App">
        <div className="App-content">
          <div className="App-header">
            <span>Fligth fares</span>
            <small>Displaying cheapest of each days in a month</small>

            {routeSelection}
          </div>

          <Paper >
            <Table >
              <TableHead>
                <TableRow>
                  <CustomTableCell>Sunday</CustomTableCell>
                  <CustomTableCell align="right">Monday</CustomTableCell>
                  <CustomTableCell align="right">Tuesday</CustomTableCell>
                  <CustomTableCell align="right">Wednesday</CustomTableCell>
                  <CustomTableCell align="right">Thursday</CustomTableCell>
                  <CustomTableCell align="right">Friday</CustomTableCell>
                  <CustomTableCell align="right">Saturday</CustomTableCell>
                </TableRow>
              </TableHead>
              <TableBody >
                <FlightsCheapestCalendar travelRoute={this.state.route}/>
              </TableBody>
            </Table>
          </Paper>

          <br/><br/>
        </div>
      </div>
    );
  }
}

export default App;
